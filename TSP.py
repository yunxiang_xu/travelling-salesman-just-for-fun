class TSPSolution():
    def __init__(self, G,n):
        self.n = n
        self.G = G
    def TSP(self):
        l = self.n - 1
        pre = ["0"*l]
        now = []
        Apre = {}
        Apre[0] = [0] + [float("inf")]*l
        Anow = {}
        for i in range(1, self.n):
            print 'computing',i,'problem                     \r',
            while pre:
                current = pre.pop()
                if current[l-1] == "1" : pass
                else:
                    it = l-1
                    while current[it] != "1" and it >=0 :
                        it -= 1
                    for j in range(it + 1, l):
                        now.append(current[:it + 1] + "0" * (j- it - 1) + "1" + "0" *(l-1-j))
                        
            for current in now:
                Anow[int(current,2)] = [None] * self.n
                for i in range(l):
                    if current[i] == '1':
                        subPro = []
                        temp = current[:i] + '0' + current[i+1:]
                        for j in range(l):
                            if temp[j] =='1':
                                subPro.append(Apre[int(temp,2)][j + 1] + self.distance(i + 1, j + 1))
                        if not subPro:
                            subPro.append(self.distance(0, i + 1))
                        Anow[int(current,2)][i + 1] = min(subPro)
            Apre = Anow
            Anow = {}
            now, pre = pre, now
        
        current = pre[0]
        mini = float("inf")
        for i in range(1, self.n):
            mini = min(mini, Apre[int(current,2)][i] + self.distance(0, i))
        print '                             \r',
        return mini
    def distance(self, a, b):
        return ((self.G[a][0] - self.G[b][0])**2 + (self.G[a][1] - self.G[b][1])**2) ** 0.5
    
if __name__ == "__main__":
    print '0 for testing...'
    choice = raw_input()
    
    if int(choice):
        f = open('_f702b2a7b43c0d64707f7ab1b4394754_tsp.txt')
    else:
        f = open('test.txt')
    n = int(f.readline())
    G = []
    for line in f:
        G.append(tuple([float(i) for i in line.split()]))
    obj = TSPSolution(G,n)

    miniVal = obj.TSP()
    print 'ans is', miniVal
    